﻿using System;

namespace Football.Data
{
    public interface IMatch
    {   
        int Id { get; set; }
        string HomeTeamName { get; set; }    
        string AwayTeamName { get; set; }
        int AwayTeamScore { get; set; }        
        int HomeTeamScore { get; set; }        
        string MatchDate { get; set; }
    }
}