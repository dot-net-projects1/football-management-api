﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Football.Data;

public class Team : ITeam
{
    private Team() {}    
    public Team(string name) 
    {
        Name = name;
    }  

    [Key]   
    public int Id { get; set; }

    [Required]
    [MaxLength(24)]    
    public string Name { get; set; }

    public int GoalsScored { get; set; } = 0;
    
    public int GoalsConceeded { get; set; } = 0;

}
