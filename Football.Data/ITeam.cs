﻿namespace Football.Data
{
    public interface ITeam
    {
        int Id { get; set; }
        string Name { get;  }

        int GoalsConceeded { get; set; } 
        int GoalsScored { get; set; }  
    }
}