using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Football.Data.Repositories
{
    public class MatchRepository : IRepository<Match, int>
    {
        private readonly FootballContext Context;
        public MatchRepository(FootballContext context) => Context = context;
        
        public async Task<IEnumerable<Match>> GetAll() => await Context.Matches.ToListAsync();
        
        public async Task<Match> GetById(int id) => await Context.Matches.FirstOrDefaultAsync(t => t.Id == id);        
             
        public async Task<Match> GetMatch(string homeTeamName, string awayTeamName, string matchDate)
        {
            return await Context.Matches.FirstOrDefaultAsync(match => match.HomeTeamName == homeTeamName && 
                                                                      match.AwayTeamName == awayTeamName && 
                                                                        match.MatchDate == matchDate);
        }

        private async Task<Team> GetTeam(string name) => await Context.Teams.FirstOrDefaultAsync(T=> T.Name == name);
        
        
        public async Task<Match> InsertNew(Match match)
        {
            match.MatchDate =  DateOnly.Parse(match.MatchDate).ToString();
            Team homeTeam = await GetTeam(match.HomeTeamName);
            Team awayTeam = await GetTeam(match.AwayTeamName);
            
            if (homeTeam?.Name == null | awayTeam?.Name == null)
                return match;

            homeTeam.GoalsScored += match.HomeTeamScore;
            homeTeam.GoalsConceeded += match.AwayTeamScore;    
            awayTeam.GoalsScored += match.AwayTeamScore;
            awayTeam.GoalsConceeded += match.HomeTeamScore;

            await Context.Matches.AddAsync(match);
            await Save();
            return match;
        }

        public async Task<Match> Update(int id, Match match)
        {
            match.MatchDate =  DateOnly.Parse(match.MatchDate).ToString();
            Match inDbMatch =  await GetById(id);
            if (inDbMatch == null)
                return match;
            
            Team homeTeam = await GetTeam(match.HomeTeamName);
            Team awayTeam = await GetTeam(match.AwayTeamName);          
            if (homeTeam?.Name == null | awayTeam?.Name == null)
                return match;
    
            homeTeam.GoalsScored += match.HomeTeamScore - inDbMatch.HomeTeamScore;
            homeTeam.GoalsConceeded += (match.AwayTeamScore - inDbMatch.AwayTeamScore > 0) ?  match.AwayTeamScore - inDbMatch.AwayTeamScore : 0;      
            awayTeam.GoalsScored += (match.AwayTeamScore - inDbMatch.AwayTeamScore > 0) ? match.AwayTeamScore - inDbMatch.AwayTeamScore : 0;
            awayTeam.GoalsConceeded += (match.HomeTeamScore - inDbMatch.HomeTeamScore > 0) ? match.HomeTeamScore - inDbMatch.HomeTeamScore : 0;

            inDbMatch.HomeTeamScore = match.HomeTeamScore;
            inDbMatch.AwayTeamScore = match.AwayTeamScore;
            await Save();
            return inDbMatch;
        }

        
        public async Task Delete(int id)
        {
            var match = await Context.Matches.FirstOrDefaultAsync(b => b.Id == id);
            if (match != null)
            {
                Context.Remove(match);
                await Save();
            }                
        }

        public async Task Save() => await Context.SaveChangesAsync();        

        public Task<Match> GetByName(string name) => null;              

    }
}