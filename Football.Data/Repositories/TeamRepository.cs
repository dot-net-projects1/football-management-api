using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Football.Data.Repositories
{
    public class TeamRepository : IRepository<Team, int>
    {
        private readonly FootballContext Context;
        public TeamRepository(FootballContext context) => Context = context;
        
        public async Task<IEnumerable<Team>> GetAll() => await Context.Teams.ToListAsync();
        
        public async Task<Team> GetById(int id) => await Context.Teams.FindAsync(id);
        
        public async Task<Team> GetByName(string name) =>
            await Context.Teams.FirstOrDefaultAsync(t => t.Name == name);
        
        public async Task<Team> InsertNew(Team entity)
        {
            if (await GetByName(entity.Name) != null)
                return new Team("Duplicates Not allowed");
            await Context.Teams.AddAsync(entity);
            await Save();
            return entity;
        }

        public async Task<Team> Update(int id, Team team)
        {
            Team inDbTeam = await GetById(id);
            if (inDbTeam == null)
                return team;

            inDbTeam.Name = team.Name;
            await Save();
            return inDbTeam;
        }

        public async Task Delete(int id)
        {
            var team = await Context.Teams.FindAsync(id);
            if (team != null) 
            {
                Context.Teams.Remove(team);
                await Save();
            } 
        }

        public async Task Save() => await Context.SaveChangesAsync();        

        public  Task<Team> GetMatch(string homeTeamName, string awayTeamName, string matchDate) =>
            throw new NotImplementedException();        

    }
}