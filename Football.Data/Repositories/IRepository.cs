using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Football.Data.Repositories
{
    public interface IRepository<T1, T2> where T1 : class
    {
        Task<IEnumerable<T1>> GetAll();
        Task<T1> GetById(T2 id);
        Task<T1> GetByName(string name);
        Task<T1> GetMatch(string homeTeamName, string awayTeamName, string matchDate);
        Task<T1> InsertNew(T1 entity);
        Task<T1> Update(int id, T1 entity);
        Task Delete(T2 id);
        Task Save();
    }
}