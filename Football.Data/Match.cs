using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using System.ComponentModel;

namespace Football.Data;

public class Match : IMatch
{    
    private Match() {}
    
    [Key]
    public int Id { get; set; }   

    [JsonConstructor]
    public Match(string homeTeamName, string awayTeamName, string matchDate)
    {
        HomeTeamName = homeTeamName;
        AwayTeamName = awayTeamName;
        MatchDate =  matchDate;
    }
  
    [Required]
    [MaxLength(24)]   
    public string HomeTeamName { get; set; }

    [Required]
    [MaxLength(24)]
    public string AwayTeamName { get ; set; }
    
    [DefaultValue(0)]    
    public int AwayTeamScore { get; set; }    
     
    [DefaultValue(0)]    
    public int HomeTeamScore { get; set; }

    [MaxLength(10)]
    public string MatchDate { get; set; }
}
