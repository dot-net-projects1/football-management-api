using System;
using System.Text;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

// Uncomment cooment sections to create migration, DB and update DB with Entity Framework.
// Replace comments to use the API with services registered in Progrm.cs, coonects by using the connection string

namespace Football.Data
{
    public class FootballContext : DbContext
    {
       // public FootballContext(){}
        
        public FootballContext(DbContextOptions<FootballContext> options) : base(options) { }

        public DbSet<Match> Matches { get; set; }

        public DbSet<Team> Teams { get; set; }

        // protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        // {
        //     if (!optionsBuilder.IsConfigured)            
        //         optionsBuilder.UseSqlite("Data Source=PATH-TO-YOUR-DATA-SOURCE");            
        // }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Match>()
                .HasAlternateKey(m => new { m.HomeTeamName, m.AwayTeamName, m.MatchDate });
            modelBuilder.Entity<Team>()
                .HasAlternateKey(t => t.Name );
        }


    }
}
