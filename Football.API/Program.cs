using Football.Core;
using Football.Data;
using Football.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// Adding dependency injection for the API
builder.Services.AddDbContextPool<FootballContext>(
    options => options.UseSqlite(builder.Configuration.GetConnectionString("DefaultConnection"))
);

builder.Services.AddScoped<IRepository<Team, int>, TeamRepository>();
builder.Services.AddScoped<IRepository<Match, int>, MatchRepository>();
builder.Services.AddScoped<ITeamFacade<Team, int>, TeamFacade>();
builder.Services.AddScoped<IMatchFacade<Match, int>, MatchFacade>();
builder.Services.AddScoped<ITeamForm, TeamForm>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseExceptionHandler("/error");
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
