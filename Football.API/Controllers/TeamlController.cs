using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Football.Core;
using Football.Data;

namespace Football.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Consumes("application/json")] 
    public class TeamController : ControllerBase
    {
        private protected ITeamFacade<Team, int> teamFacade;
        private protected ITeamForm teamForm;

        public TeamController(ITeamFacade<Team, int> facade, ITeamForm tform)
        {
            teamFacade = facade;
            teamForm = tform;
        }

        [HttpGet]   // GET /api/Team
        public async Task<IEnumerable<Team>> Index() =>  await teamFacade.GetAll();


        [HttpGet("{value}")]    // GET /api/team/{id} .. Get Team with id, // GET /api/team/{name} .. Get team by name
        public async Task<Team> Team(string value)
        {
            int id = 0;
            bool success = int.TryParse(value, out id);
            if (success)
                return  await teamFacade.GetById(id); 
            return await teamFacade.GetByName(value);
        }         
        
        
        [HttpPost("create")]    // POST /api/team/create   
        public async Task<Team> Create(Team teamName) => await teamFacade.Insert(teamName);    
        
        [HttpGet("form/{name}")]   // GET /api/team/form/{team-name}
        public string Form(string name)
        {
            return teamForm.HomeForm(name) + teamForm.AwayForm(name);
        }

        [Route("/error")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IActionResult HandleError() => Problem();
        

        // GET Team home form

        // Get Team away form

        // Get Team combined form
    }
}