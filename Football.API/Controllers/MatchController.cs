using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Football.Core;
using Football.Data;

namespace Football.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Consumes("application/json")] 
    public class MatchController : ControllerBase
    {
        private readonly IMatchFacade<Match, int> matchFacade;
        
        public MatchController(IMatchFacade<Match, int> mfacade) => matchFacade = mfacade;
        
       
        [HttpGet]   // GET /api/match
        public async Task<IEnumerable<Match>> Index()=> await matchFacade.GetAll();   
        

        [HttpGet("{id}")]   // GET /api/match/{id}
        public async Task<Match> Match(int id) => await matchFacade.GetById(id);
        
                       
        [HttpPost("create")]  // POST /api/match/create   
        public async Task<Match> Create(Match match) => await matchFacade.Insert(match);
                
        
        [HttpPut("update")]  // PUT /api/match/update                
        public async Task<Match> Update([Bind("Id, HomeTeamName, AwayTeamName, MatchDate, HomeTeamScore, AwayTeamScore")] Match match)
                                        => await matchFacade.Update(match);            
        
        [HttpDelete("{id}")]    // DELETE /api/match/{id}
        public async Task  Delete(int id)
        {
            await matchFacade.Delete(id);
            await matchFacade.Save();
        }

               
        [Route("/error")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IActionResult HandleError() => Problem();
    }
}