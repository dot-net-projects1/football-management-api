This is a solution to a Development Test

Requirements:

As a football manager, I would like to be able to be able to record teams results and then query which teams in the league have the best current form, so that I can accurately identify the quality of the opposition.
Details:

Form should be calculated for home matches, away matches and all matches for a particular team – that is 3 separate form calculations For each form calculation
First convert the score into its respective home (H), win (W) or draw (D)
Then order this result descending – so the most resent result is first.


Example:
-------------------------------------------------------
29th Dec West Brom 2 -0 Man Utd
-------------------------------------------------------
22nd Dec Burton 2 - 2 West Brom
-------------------------------------------------------
15th Dec West Brom 1 - 1 Gillingham
-------------------------------------------------------
8th Dec Wolves 0 - 3 West Brom
-------------------------------------------------------
1st Dec Tamworth 1 - 0 West Brom
-------------------------------------------------------

Home form for West Brom would be: WD
Away form for West Brom would be: DWL
Total form for West Brom would be: WDDWL

Guidance:
You will require an empty Visual Studio solution, called ‘Football’, which will contain your projects.
Add a C# .NET Standard class library project, called ‘Football.Data’, with appropriate models and classes, utilising the Repository pattern to allow data access via Entity Framework Core.

Add a C# .NET Standard class library project, called ‘Football.Core, with appropriate classes facilitating the Façade pattern, containing the logic to call the appropriate repository methods. Factor additional patterns to separate the Form calculations from the Façade logic.

Add a C# MsTest .NET Core project, called ‘Football.Core.Test’, with appropriate tests to ensure the quality of your Football.Core project classes.

Add a C# .NET Core Web Application API project, called ‘Football.API’, with a controller containing 2 restful endpoints which will call the appropriate methods on the Football.Core Façade. Any application errors must be caught here and only here, responding appropriately.



##  The Solution

    The solution is done using .NET 6.0, Entity Framework, and SQLite to create a class library, Models, and API controllers to meet the requirements. 

    To run the solution:

    - install .NET6 Core (SDK), .NET Run-time and Entity   Framework, and choise of Database/driver

    - run "dotnet restore command" from the command line inside the solution folders  
    
    - add your db connection string to FootballContext.cs, and API settings.json
    
    - create and update DB tables using the migrations by running -- "dotnet ef database update" from the command line inside Football.Data. If you modify the model classes, delete the migrations, create new migrations before running "dotnet ef database update" 

    - cd to Football.API folder, run "dotnet build" and "dotnet run" 

    - test with a API client application, API consumes only application/json at this stage

## API endpoints

    GET /api/Team                       get all teams
    GET /api/team/{id}                  get team with the give id, 
    GET /api/team/{name}                get team by name
    GET /api/team/form/{team-name}      return the combined Form (Home + Away form) of the team with the given name
    POST /api/team/create               create a new team with the given "Name"                
    
    
    GET /api/match                      get all matches
    GET /api/match/{id}                 get a match with the given id
    POST /api/match/create              creates a new match and returns it with its given id which must be greater than 1
    PUT /api/match/update               updates the scores of a given match
    DELETE /api/match/{id}              deletes a match with the given id


#### To Do's
-   order match form in descending order so the most recent result is first
-   create unit tests for the API
