using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Football.Data;
using Football.Data.Repositories;

namespace Football.Core
{
    public class TeamForm : ITeamForm
    {   
        private readonly IRepository<Match, int> matchRepository;
        public TeamForm(IRepository<Match, int> mRepo) => matchRepository = mRepo;
      
        public string HomeForm(string teamName)
        {
            string formString = "";
            List<Match>  matches = matchRepository.GetAll().Result.ToList();            
            foreach (var match in matches)            
                if (match.HomeTeamName == teamName)                                             
                    formString += HomeTeamResult(match);                              
            
            return formString;  
        }

        private string HomeTeamResult(Match match)
            => match.HomeTeamScore > match.AwayTeamScore ? "W" : match.HomeTeamScore < match.AwayTeamScore ? "L" : "D";
        

        public string AwayForm(string teamName)
        {
            string formString = "";
            List<Match>  matches = matchRepository.GetAll().Result.ToList();
            
            foreach (var match in matches)            
                if (match.AwayTeamName == teamName)                                             
                   formString += HomeTeamResult(match);                 
            
            return formString;  
        } 

        private string AwayTeamResult(Match match) 
            => match.HomeTeamScore > match.AwayTeamScore ? "L" : match.HomeTeamScore < match.AwayTeamScore ? "W" : "D";

    }
}