using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Football.Core
{
    public interface ITeamForm
    {
        string HomeForm(string teamName);

        string AwayForm(string teamName);
    }
}