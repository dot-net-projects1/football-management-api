using System;
using System.Collections.Generic;

namespace Football.Core
{
    public interface IMatchFacade<T1, T2> where T1 : class
    {        
        Task<IEnumerable<T1>> GetAll();        
        Task<T1> GetMatch(T1 match); 
        Task<T1> GetById(int id);

        Task<T1> Insert(T1 match);        
        Task<T1> Update(T1 match);        
        Task Delete(T2 id);
        Task Save();
    }
}