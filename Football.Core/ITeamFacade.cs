using System;
using System.Collections.Generic;

namespace Football.Core
{
    public interface ITeamFacade<T1, T2> where T1 : class
    {        
        Task<IEnumerable<T1>> GetAll();        
        Task<T1> GetById(int id);
        Task<T1> GetByName(string name);    
        Task<T1> Insert(T1 entity);        
        Task<T1> Update(T1 newName);        
        Task Delete(T2 id);
        Task Save();
    }
}