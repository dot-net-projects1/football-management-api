﻿using System;
using Football.Data;
using Football.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Football.Core
{
    public class MatchFacade : IMatchFacade<Match, int>
    {
        protected readonly FootballContext Context; 
        private readonly IRepository<Match, int> matchRepository;   

        public MatchFacade(IRepository<Match, int> mRepo) => matchRepository = mRepo;

        public async Task<IEnumerable<Match>> GetAll() => await matchRepository.GetAll();

        public async Task<Match> GetMatch(Match match)
                                 => await matchRepository.GetMatch(match.HomeTeamName, match.AwayTeamName, match.MatchDate);
        public async Task<Match> GetById(int id) => await matchRepository.GetById(id);
        
        public async Task<Match> Insert(Match match) => await matchRepository.InsertNew(match);

        public Task Save() => matchRepository.Save();
        
        public async Task Delete(int id) => await matchRepository.Delete(id);      
        public async Task<Match> Update(Match match) => await matchRepository.Update(match.Id, match);
         

    }
}