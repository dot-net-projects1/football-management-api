using System;
using Football.Data;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using Football.Data.Repositories;

namespace Football.Core
{    public class TeamFacade : ITeamFacade<Team, int>
    {        
        private readonly IRepository<Team, int> teamRepository;
        
        public TeamFacade(IRepository<Team, int> tr) => teamRepository = tr;

        public async Task<IEnumerable<Team>> GetAll() => await teamRepository.GetAll();

        public async Task<Team> GetById(int id) => await teamRepository.GetById(id);

        public async Task<Team> GetByName(string name) => await teamRepository.GetByName(name);
        
        public async Task<Team> Insert(Team teamName) => await teamRepository.InsertNew(teamName);
        
        public async Task<Team> Update(Team team) => 
                                        await teamRepository.Update(team.Id, team);        

        public Task Save() =>  teamRepository.Save();        

        public async Task Delete(int id) => await teamRepository.Delete(id);    

    }
}